﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitSpin : MonoBehaviour {

    public Transform playerTransform;
    public float SpinRate = 1.0f;
    private bool isSpinning = false;
    public float timeToSpin = 5;
    private SoundEffectManager sound;
    public Transform exitTransform;
    public int MaxLevel = 2;
    public int SceneIndex = 1;

    private void Start()
    {
        sound = GameObject.FindObjectOfType<SoundEffectManager>();
        exitTransform = gameObject.transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            /* play sound. */
            sound.PlayEffect(SoundEffectManager.SoundEffects.Exit);

            /* spin the player */
            SpinPlayer();
        }
    }

    private void Update()
    {
        if (isSpinning)
        {
            /* set the position of the character to that of the exit. */
            playerTransform.position = exitTransform.position;
            playerTransform.Rotate(0f, SpinRate * Time.deltaTime * 7 , 0f);

           

            if(Time.time >= timeToSpin)
            {
                isSpinning = false;
                Destroy(playerTransform.gameObject);

                /* restart the level TODO: move to game manager. */
                if(SceneIndex < MaxLevel)
                {
                    SceneManager.LoadScene(SceneIndex+=1);
                }
                else
                {
                    SceneManager.LoadScene(1);
                }
            }
        }
    }

    private void SpinPlayer()
    {
      
        isSpinning = true;
        timeToSpin = Time.time + timeToSpin;
    }

}
