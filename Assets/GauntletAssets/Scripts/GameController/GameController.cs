﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    PlayerInventory inventory;
    int healthDecayPerSecond = 1;
    private SoundEffectManager soundManager;

    float nextDecay;

	// Use this for initialization
	void Start () {
        inventory = GameObject.FindObjectOfType<PlayerInventory>();
        soundManager = GameObject.FindObjectOfType<SoundEffectManager>();
        soundManager.PlayEffect(SoundEffectManager.SoundEffects.Welcome);
 	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!GameOver())
            HealthDecay();
        else
            Application.Quit();
	}

    /* Remove one health every second. */
    private void HealthDecay()
    {
        if(nextDecay <= Time.time)
        {
            inventory.TakeDamage(healthDecayPerSecond);
            nextDecay = Time.time + healthDecayPerSecond;
        }
            
    }

    private bool GameOver()
    {
        return inventory.Health <= 0;
    }
}
