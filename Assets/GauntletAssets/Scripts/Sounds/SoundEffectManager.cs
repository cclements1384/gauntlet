﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectManager : MonoBehaviour {

    AudioSource audioSource;
    /* Add new Audio Clips here as public */
    public AudioClip keyPickup;
    public AudioClip openGate;
    public AudioClip coinPickup;
    public AudioClip playerHurt;
    public AudioClip ghostHurt;
    public AudioClip exit;
    public AudioClip welcome;
    public AudioClip hint;
    public AudioClip spell;

    /* Add new Clip Enum here */
    public enum SoundEffects
    {
        PickupKey = 0,
        OpenGate = 1,
        PickupCoins = 2,
        PlayerHurt = 3,
        GhostHurt = 4,
        Exit = 5,
        Welcome = 6,
        Hint = 7,
        Spell = 8
    }


    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }


	public void PlayEffect(SoundEffects clipEnum = SoundEffects.PickupKey)
    {
        SelectClip(clipEnum);
        if(!audioSource.isPlaying)
            audioSource.Play();
    }

    private void SelectClip(SoundEffects clipEnum)
    {
        /* add code to load new audio clips here */
        if (clipEnum == SoundEffects.PickupKey)
            audioSource.clip = keyPickup;
        if (clipEnum == SoundEffects.OpenGate)
            audioSource.clip = openGate;
        if (clipEnum == SoundEffects.PickupCoins)
            audioSource.clip = coinPickup;
        if (clipEnum == SoundEffects.PlayerHurt)
            audioSource.clip = playerHurt;
        if (clipEnum == SoundEffects.GhostHurt)
            audioSource.clip = ghostHurt;
        if (clipEnum == SoundEffects.Exit)
            audioSource.clip = exit;
        if (clipEnum == SoundEffects.Welcome)
            audioSource.clip = welcome;
        if (clipEnum == SoundEffects.Hint)
            audioSource.clip = hint;
        if (clipEnum == SoundEffects.Spell)
            audioSource.clip = spell;
    }
}
