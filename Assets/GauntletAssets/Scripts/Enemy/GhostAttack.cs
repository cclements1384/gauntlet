﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAttack : MonoBehaviour {

    public int Damage;
    private PlayerInventory player;
    private SoundEffectManager sounds;

    private void Start()
    {
        player = GameObject.FindObjectOfType<PlayerInventory>();
        sounds = GameObject.FindObjectOfType<SoundEffectManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            /* deal damage to the player */
            player.TakeDamage(Damage);

            /* play some effect? */
            sounds.PlayEffect(SoundEffectManager.SoundEffects.PlayerHurt);
            
            /* destroy this ghost */
            Destroy(gameObject);
        }
    }
}
