﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMovement : MonoBehaviour
{

    Transform playerTransform;
    public float speed;
    public float rotateSpeed;


    // Use this for initialization
    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        RotateToPlayer();
        Move();
    }

    private void RotateToPlayer()
    {
        if(playerTransform != null)
        {
            /* this is the difference between the player and the this ghost */
            Vector3 directionToTarget = playerTransform.position - this.transform.position;

            /* look in the direction of the player */
            this.transform.rotation = Quaternion.LookRotation(directionToTarget);
        }
    }

    private void Move()
    {
        /* apply the movement. */
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
