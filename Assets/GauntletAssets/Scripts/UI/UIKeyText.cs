﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIKeyText : MonoBehaviour
{
    private PlayerInventory playerInventory;
    Text keyText;
    Text healthText;
    Text goldText;

    // Use this for initialization
    void Start()
    {
        playerInventory = GameObject.FindObjectOfType<PlayerInventory>();
        keyText = GameObject.Find("KeysText").GetComponent<Text>();
        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        goldText = GameObject.Find("ScoreText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        DrawHUD(); 
    }

    void DrawHUD()
    {
        keyText.text = string.Format("Keys: {0}",
            playerInventory.Keys.ToString());

        healthText.text = string.Format("Health: {0}",
            playerInventory.Health.ToString());

        goldText.text = string.Format("Score: {0}",
            playerInventory.Gold.ToString());
    }
}
