﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenuManager : MonoBehaviour {

	public void OnStartGame_Click()
    {
        SceneManager.LoadScene(1);
    }
}
    