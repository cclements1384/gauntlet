﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHint : MonoBehaviour
{

    public float timeToShowHint = 2f;

    private float timeToStopShowingHint;
    private Canvas canvas;
    private bool visible = false;
    private Text hintText;
    private SoundEffectManager soundManager;

    private void Start()
    {
        canvas = GameObject.Find("HintCanvas").GetComponent<Canvas>();
        hintText = GameObject.Find("HintText").GetComponent<Text>();
        soundManager = GameObject.FindObjectOfType<SoundEffectManager>();
        canvas.enabled = false;
    }

    public void ShowHint(string text)
    {
        soundManager.PlayEffect(SoundEffectManager.SoundEffects.Hint);
        ShowCanvas(true);
        hintText.text = text;
        timeToStopShowingHint = Time.time + timeToShowHint;
    }

    private void Update()
    {
        if (Time.time >= timeToStopShowingHint)
        {
            ShowCanvas(false);
        }
    }

    private void ShowCanvas(bool visible)
    {
        canvas.enabled = visible;
        visible = visible;
    }
}
