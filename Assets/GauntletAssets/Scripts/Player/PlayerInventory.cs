﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {

    private GameOverManager game;

    public int Keys;
    public int Gold;
    public int Health;
    public int Potions;

  

	// Use this for initialization
	void Start () {
        game = GameObject.FindObjectOfType<GameOverManager>();	
	}
	
	// Update is called once per frame
	void Update () {

	}

    public bool HasKey()
    {
        return Keys > 0;
    }

    public void ConsumeKey()
    {
        if (Keys > 0)
            Keys--;
    }

    public void AddKey()
    {
        Keys++;
    }

    public void TakeDamage(int pointsToDeduct)
    {
        Health -= pointsToDeduct;

        if(Health <= 0)
        {
            game.GameOver();
        }
    }

    public void AddScore(int amountToAdd)
    {
        Gold += amountToAdd;
    }

    public void AddHealth(int amountToAdd)
    {
        Health += amountToAdd;
    }

    public void AddPotions(int amountToAdd)
    {
        Potions += amountToAdd;
    }

    public bool HasPotion()
    {
        return Potions > 0;
    }

    public void ConsumePotion()
    {
        Potions--;
    }

}
