﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour {
    /* this class does movement based on the rigid body class */

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float roateRate = 360;

    private Vector3 movement = Vector3.zero;
    private Rigidbody rb;
    Quaternion rotation;
    Animator anim;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rotation = new Quaternion();
        anim = GameObject.Find("guard_mesh").GetComponent<Animator>();
    }

    private void Update()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        /* move the character */
        Move(h, v);

        /* Animate the character */
        Animate(h,v);
    }

    private void Move(float h, float v)
    {
        if (v == 1 && h == 1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, 45, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(h) * speed * Time.deltaTime);
        }
        else if (v == 1 && h == -1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, -45, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(h) * speed * Time.deltaTime);

        }
        else if (v == -1 && h == -1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, -120, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(h) * speed * Time.deltaTime);

        }
        else if (v == -1 && h == 1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, 120, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(h) * speed * Time.deltaTime);
        }
        else if (v == 1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, 0, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(v) * speed * Time.deltaTime);

        }
        else if (v == -1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, 180, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(v) * speed * Time.deltaTime);

        }
        else if (h == 1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, 90, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(h) * speed * Time.deltaTime);

        }
        else if (h == -1)
        {
            /* rotate the vector to the right */
            rotation = Quaternion.Euler(0, -90, 0);
            transform.rotation = rotation;
            transform.Translate(Vector3.forward * Mathf.Abs(h) * speed * Time.deltaTime);

        }
    }
    
    private void Animate(float h, float v)
    {
        bool isRunning = h != 0 || v != 0;
        anim.SetBool("IsRunning", isRunning);
    }
}
