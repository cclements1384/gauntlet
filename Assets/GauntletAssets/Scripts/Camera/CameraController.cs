﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public float smoothing = 5.0f;

    float offset;

    private void Start()
    {
        /* get the offset from the camera to the player */
        offset = 20f; 
    }

    private void LateUpdate()
    {
        if(target != null)
        {
            Vector3 targetCamPos = new Vector3(target.position.x, offset, target.position.z - 8f);
            transform.position = Vector3.Lerp(transform.position,
                targetCamPos, smoothing * Time.deltaTime);
        }
      
    }

}
