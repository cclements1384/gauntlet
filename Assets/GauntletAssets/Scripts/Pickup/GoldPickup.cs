﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldPickup : MonoBehaviour {

    PlayerInventory inventory;
    public int Amount;
    SoundEffectManager sounds;
    private UIHint hint;
    public bool ShowHint = false;


    private void Start()
    {
        inventory = GameObject.FindObjectOfType<PlayerInventory>();
        sounds = GameObject.FindObjectOfType<SoundEffectManager>();
        hint = GameObject.FindObjectOfType<UIHint>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
            PickUpGold();
    }

    private void PickUpGold()
    {
        /* Show the hint */
        if(ShowHint)
            ShowHintDialog("Gold adds 100 to score.");

        /* add the gold to the player inventory */
        AddToPlayerScore();

        /* play the sounds effect */
        PlaySoundEffect();

        /* remove the object from the game */
        Destroy(gameObject);
    }

    private void PlaySoundEffect()
    {
        SoundEffectManager.SoundEffects effect;

        if (ShowHint)
            effect = SoundEffectManager.SoundEffects.Hint;
        else
            effect = SoundEffectManager.SoundEffects.PickupCoins;

        sounds.PlayEffect(effect);
    }

    private void ShowHintDialog(string hintText)
    {
        hint.ShowHint(hintText);
    }

    private void AddToPlayerScore()
    {
        inventory.AddScore(Amount);
    }
}
