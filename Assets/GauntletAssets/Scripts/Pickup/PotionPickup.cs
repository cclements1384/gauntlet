﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollectable
{
    void Collect();
    void PlaySoundEffect();
    void RemoveCollectable();
}

public interface IHintable
{
    void ShowHintDialog(string hintText);
}

public class PotionPickup : MonoBehaviour , 
    ICollectable,
    IHintable
{
    public int amount = 1;
    public bool showHint = false;

    private PlayerInventory inventory;
    private UIHint hint;
    private SoundEffectManager sounds;

    private void Awake()
    {
        inventory = GameObject.FindObjectOfType<PlayerInventory>();
        hint = GameObject.FindObjectOfType<UIHint>();
        sounds = GameObject.FindObjectOfType<SoundEffectManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
            Collect();
    }

    public void Collect()
    {
        if (showHint)
            ShowHintDialog("Collect potions to use magic.");
        AddPotionToInventory();
        PlaySoundEffect();
        RemoveCollectable();
    }
        
    private void AddPotionToInventory()
    {
        inventory.AddPotions(amount);
    }

    public void PlaySoundEffect()
    {
        if (showHint)
            sounds.PlayEffect(SoundEffectManager.SoundEffects.Hint);
        else
            sounds.PlayEffect(SoundEffectManager.SoundEffects.PickupKey);
        /* TODO: Replace with potion pickup sound. */
    }

    public void RemoveCollectable()
    {
        Destroy(gameObject);
    }

    public void ShowHintDialog(string hintText)
    {
        hint.ShowHint(hintText);
    }
}
