﻿using UnityEngine;
using System.Collections;

public class KeyPickup : MonoBehaviour
{

    public float rotationspeed;
    SoundEffectManager soundEffects;
    PlayerInventory inventory;
    UIHint hint;
    public bool ShowHint = false;
    
    // Use this for initialization
    void Start()
    {
        soundEffects = GameObject.FindObjectOfType< SoundEffectManager>();
        inventory = GameObject.FindObjectOfType<PlayerInventory>();
        hint = GameObject.FindObjectOfType<UIHint>();
    }

    private void FixedUpdate()
    {
        transform.Rotate(new Vector3(0.0f, rotationspeed, 0.0f));
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider triggeringObject)
    {
        /* the player collided with the pickup item. */
        if (triggeringObject.tag == "Player")
        {
            inventory.AddKey();
            PlaySoundEffect();
            DisablePickup();
            if(ShowHint)
                ShowHintDialog("Use keys to open locked gates.");
        }
    }

    void PlaySoundEffect()
    {
        SoundEffectManager.SoundEffects effect;

        if (ShowHint)
            effect = SoundEffectManager.SoundEffects.Hint;
        else
            effect = SoundEffectManager.SoundEffects.PickupKey;

        soundEffects.PlayEffect(effect);
    }

    void DisablePickup()
    {
        Destroy(gameObject);
    }

    private void ShowHintDialog(string hintText)
    {
        hint.ShowHint(hintText);
    }
}
