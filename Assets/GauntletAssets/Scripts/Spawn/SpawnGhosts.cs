﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGhosts : MonoBehaviour {

    public Transform spawnLocation;
    public GameObject ghost;
    public float spawnRate;
    public float lastSpawn;
    private float nextSpawn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        
        if(Time.time > nextSpawn)
        {
            Spawn();
        }
          
	}

    void Spawn()
    {
        Instantiate(ghost, spawnLocation.position, spawnLocation.rotation);
        nextSpawn = Time.time + spawnRate;
    }
}
