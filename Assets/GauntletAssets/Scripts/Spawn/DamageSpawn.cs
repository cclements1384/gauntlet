﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSpawn : MonoBehaviour {

    private SoundEffectManager soundManager;

    private void Start()
    {
        soundManager = GameObject.FindObjectOfType<SoundEffectManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Weapon"))
        {
            /* play sounds */
            soundManager.PlayEffect(SoundEffectManager.SoundEffects.GhostHurt);
            
            /* destoy the spawn */
            Destroy(gameObject);

            /* destroy the weapon */
            Destroy(other.gameObject);
        }
    }
}
