﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordSpawn : MonoBehaviour {

    public GameObject flyingSword;
    public float rateOfFire = .5f;
    private SwordSpawn swordSpawn;
    private PlayerInventory inventory;
    private Transform playerTransform;
    public GameObject particles;
    private SoundEffectManager sound;

    private float nextFireTime;

	// Use this for initialization
	void Start () {
        swordSpawn = GameObject.FindObjectOfType<SwordSpawn>();
        inventory = GameObject.FindObjectOfType<PlayerInventory>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        sound = GameObject.FindObjectOfType<SoundEffectManager>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1") || Input.GetButton("Jump"))
        {
            if(Time.time >= nextFireTime)
            {
                SpawnFlyingSword();
            }
        }
        if (Input.GetButton("Fire2"))
        {
            /* if player has potion, use it */
            if (inventory.HasPotion())
            {
                UseMagic();
            }
        }
	}

    private void UseMagic()
    {

        /* show the particle effect. */
        particles.SetActive(true);

        /* play the sound effect */
        PlaySoundEffect();

        /* cast an overlay sphere and get all
         * the enemies touching or in the sphere */
        var enemies = Physics.OverlapSphere(playerTransform.position , 5);

        /* check the returned coliders */
        foreach (var collider in enemies)
        {
            /* if the collider is tagged as enemy, detroy it. TODO: deal damage. */
            if (collider.CompareTag("Enemy"))
            {
                Destroy(collider.gameObject);
            }
        }
        
        /* burn the potion. */
        inventory.ConsumePotion();

        /* turn off the particle effect. */
        StartCoroutine(Pause(2));

      
    }

    IEnumerator Pause(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        particles.SetActive(false);
    }

    private void SpawnFlyingSword()
    {
        Quaternion rotation = transform.rotation;
            
        Instantiate(flyingSword,
            swordSpawn.transform.position,
            rotation);

        /* set the rate of fire delay */
        nextFireTime = Time.time + rateOfFire;
    }

    private void PlaySoundEffect()
    {
        sound.PlayEffect(SoundEffectManager.SoundEffects.Spell);
    }
}
