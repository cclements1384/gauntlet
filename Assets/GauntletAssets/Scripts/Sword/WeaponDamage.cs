﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDamage : MonoBehaviour {


    private SoundEffectManager sounds;
    private PlayerInventory playerInventory;

    private void Start()
    {
        sounds = GameObject.FindObjectOfType<SoundEffectManager>();
        playerInventory = GameObject.FindObjectOfType<PlayerInventory>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            /* play the enemy damage sound effect */
            sounds.PlayEffect(SoundEffectManager.SoundEffects.GhostHurt);

            /* deal damage to the Enemy */
            Destroy(other.gameObject);

            /* get points from the object. TODO: read this from the object that is destroyed. */
            playerInventory.AddScore(10);
        }

        /* destroy this instance of the weapon. */
        Destroy(gameObject);
    }


}
