﻿using UnityEngine;
using System.Collections;

public class OpenGate : MonoBehaviour
{

    private Animator animator;
    private bool isOpen = false;
    private bool isClosed = true;
    private PlayerInventory inventory;
    private SoundEffectManager sounds;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        inventory = GameObject.FindObjectOfType<PlayerInventory>();
        sounds = GameObject.FindObjectOfType<SoundEffectManager>();
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider triggeringEntity)
    {
        if (triggeringEntity.tag == "Player" && inventory.HasKey())
        {
            inventory.ConsumeKey();
            sounds.PlayEffect(SoundEffectManager.SoundEffects.OpenGate);
            animator.SetBool("isOpening", true);

        }
    }
}
