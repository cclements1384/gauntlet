﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour {

    public int value;
    public bool showHint = false;

    private UIHint hint;
    private PlayerInventory player;
    private SoundEffectManager sounds;

    private void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerInventory>();
        hint = GameObject.FindObjectOfType<UIHint>();
        sounds = GameObject.FindObjectOfType<SoundEffectManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AddHealth();
            PlaySoundEffect();
            if (showHint)
            {
                ShowHintDialog("Food adds 100 health.");
            }
            Destroy(gameObject);
        }
    }

    private void AddHealth()
    {
        player.AddHealth(value);
    }

    private void PlaySoundEffect()
    {
        SoundEffectManager.SoundEffects effect;

        if (showHint)
            effect = SoundEffectManager.SoundEffects.Hint;
        else
            effect = SoundEffectManager.SoundEffects.PickupKey;

        sounds.PlayEffect(effect);
    }

    private void ShowHintDialog(string hintMessage)
    {
        hint.ShowHint(hintMessage);
    }
}
